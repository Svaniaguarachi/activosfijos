<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create(); //crea el usuario admin desde database/factories/UserFactory.php
        
        //Registramos ciudades
        \App\Models\Ciudad::create(["nombre"=>"La Paz"]);
        \App\Models\Ciudad::create(["nombre"=>"Santa Cruz de la Sierra"]);
        \App\Models\Ciudad::create(["nombre"=>"Cochabamba"]);
        \App\Models\Ciudad::create(["nombre"=>"El Alto"]);
        \App\Models\Ciudad::create(["nombre"=>"Sucre"]);
        \App\Models\Ciudad::create(["nombre"=>"Potosí"]);
        \App\Models\Ciudad::create(["nombre"=>"Tarija"]);
        \App\Models\Ciudad::create(["nombre"=>"Oruro"]);
        \App\Models\Ciudad::create(["nombre"=>"Trinidad "]);
        \App\Models\Ciudad::create(["nombre"=>"Cobija "]);
        \App\Models\Ciudad::create(["nombre"=>"Quillacollo"]);
        \App\Models\Ciudad::create(["nombre"=>"Montero "]);
        \App\Models\Ciudad::create(["nombre"=>"Villazón"]);
        \App\Models\Ciudad::create(["nombre"=>"Tupiza"]);
        \App\Models\Ciudad::create(["nombre"=>"Viacha"]);
        \App\Models\Ciudad::create(["nombre"=>"Warnes"]);

        //Registramos estados
        \App\Models\Estado::create(["descrip"=>"Nuevo"]);
        \App\Models\Estado::create(["descrip"=>"Muy buen estado"]);
        \App\Models\Estado::create(["descrip"=>"Buen estado"]);
        \App\Models\Estado::create(["descrip"=>"Regular"]);
        \App\Models\Estado::create(["descrip"=>"Malo en Uso"]);
        \App\Models\Estado::create(["descrip"=>"Malo en Desuso"]);
        \App\Models\Estado::create(["descrip"=>"Baja"]);

        //Registramos grupos
        \App\Models\Grupo::create(["descrip"=>"Terrenos y bienes naturales", "vidautil"=>40]);
        \App\Models\Grupo::create(["descrip"=>"Construcciones", "vidautil"=>40]);
        \App\Models\Grupo::create(["descrip"=>"Instalaciones técnicas", "vidautil"=>8]);
        \App\Models\Grupo::create(["descrip"=>"Maquinaria", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Mobiliario", "vidautil"=>10]);
        \App\Models\Grupo::create(["descrip"=>"Muebles y ensere de oficina", "vidautil"=>10]);
        \App\Models\Grupo::create(["descrip"=>"Equipo de comunicación", "vidautil"=>10]);
        \App\Models\Grupo::create(["descrip"=>"Equipos para procesos informáticos", "vidautil"=>4]);
        \App\Models\Grupo::create(["descrip"=>"Elementos de transporte", "vidautil"=>8]);
        \App\Models\Grupo::create(["descrip"=>"Marcas registradas", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Patentes", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Derechos de autor", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Franquicias", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Licencias y permisos", "vidautil"=>5]);
        \App\Models\Grupo::create(["descrip"=>"Otros", "vidautil"=>8]);

        //Registramos Oficinas
        \App\Models\Oficina::create(["codigo"=>"CODIR", "nombre"=>"Comité de Dirección"]);
        \App\Models\Oficina::create(["codigo"=>"CCO", "nombre"=>"Departamento comercial"]);
        \App\Models\Oficina::create(["codigo"=>"DDCC", "nombre"=>"Departamento de compras"]);
        \App\Models\Oficina::create(["codigo"=>"DDCG", "nombre"=>"Departamento de control de gestión"]);
        \App\Models\Oficina::create(["codigo"=>"COO", "nombre"=>"Departamento de logística y operaciones"]);
        \App\Models\Oficina::create(["codigo"=>"CMO", "nombre"=>"Departamento de marketing"]);
        \App\Models\Oficina::create(["codigo"=>"RRHH", "nombre"=>"Departamento de recursos humanos"]);
        \App\Models\Oficina::create(["codigo"=>"CFO", "nombre"=>"Departamento financiero"]);
        \App\Models\Oficina::create(["codigo"=>"DG", "nombre"=>"Dirección general"]);

        //creamos una carpeta dentro de public/storage, para subir las fotos de los responsables
        $carpeta = public_path("storage/responsables");
        if (!file_exists($carpeta)) {
            \File::makeDirectory($carpeta);
        }
        $foto1 = time()."_responsables.jpg";
        sleep(1);
        $foto2 = time()."_responsables.jpg";
        \File::copy(public_path('assets/imagen/responsable1.jpg'), public_path("storage/responsables/".$foto1));
        \File::copy(public_path('assets/imagen/responsable2.jpg'), public_path("storage/responsables/".$foto2));
        //Registramos Responsables
        \App\Models\Responsable::create(["ciudad_id"=>1, "nombre"=>"Adriana Carolina Hernandez Monterroza", "ci"=>8520147, "foto"=>$foto1]);
        \App\Models\Responsable::create(["ciudad_id"=>2, "nombre"=>"Oscar Fabian Castellanos Rojas", "ci"=>8747401, "foto"=>$foto2]);

        //creamos una carpeta dentro de public/storage, para subir las fotos de los activos
        $carpeta = public_path("storage/activos");
        if (!file_exists($carpeta)) {
            \File::makeDirectory($carpeta);
        }
        $foto_activo = time()."_activos.jpg";
        \File::copy(public_path('assets/imagen/activos.jpg'), public_path("storage/activos/".$foto_activo));
        //Registramos Activos
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>2, "estado_id"=>3, "codigo"=>"C-100", "descrip"=>"Anilladora", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>2, "estado_id"=>3, "codigo"=>"C-101", "descrip"=>"Cafetera electrica", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>2, "estado_id"=>3, "codigo"=>"C-102", "descrip"=>"Caja fuerte", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>2, "estado_id"=>3, "codigo"=>"C-103", "descrip"=>"Cajonera", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>2, "estado_id"=>3, "codigo"=>"C-104", "descrip"=>"Calculadora", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-105", "descrip"=>"Catre", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-106", "descrip"=>"Credenza", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-107", "descrip"=>"Destructur de papel", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-108", "descrip"=>"Dispensador de agua", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-109", "descrip"=>"Escalera", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-110", "descrip"=>"Escritorio", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>3, "estado_id"=>3, "codigo"=>"C-111", "descrip"=>"Estación de trabajo", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-112", "descrip"=>"Estante", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-113", "descrip"=>"Fotocopiadora", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-114", "descrip"=>"Gavetero", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-115", "descrip"=>"Guillotina", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-116", "descrip"=>"Lampara", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-117", "descrip"=>"Mampara", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=>4, "estado_id"=>3, "codigo"=>"C-118", "descrip"=>"Maquina de escribir", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 4, "estado_id"=>3, "codigo"=>"C-119", "descrip"=>"Mesa", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-120", "descrip"=>"Modular", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-121", "descrip"=>"Panel informativo", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-122", "descrip"=>"Perchero", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-123", "descrip"=>"Revistero", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-124", "descrip"=>"Secadora de manos", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 5, "estado_id"=>3, "codigo"=>"C-125", "descrip"=>"Silla", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-126", "descrip"=>"Sillon", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-127", "descrip"=>"Sofa", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-128", "descrip"=>"Somier", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-129", "descrip"=>"Taburete", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-130", "descrip"=>"Tandem", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>6, "oficina_id"=> 6, "estado_id"=>3, "codigo"=>"C-131", "descrip"=>"Tarima", "precio"=>150, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-141", "descrip"=>"Central telefonica", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-142", "descrip"=>"Consola de sonido", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-143", "descrip"=>"Consola de video", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-144", "descrip"=>"Facsimile", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-145", "descrip"=>"Filmadora", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-145", "descrip"=>"Grabadora digital", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-146", "descrip"=>"Grabador", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-147", "descrip"=>"Handy", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-148", "descrip"=>"Microfono", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-149", "descrip"=>"Parlante", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-150", "descrip"=>"Telefono", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-151", "descrip"=>"Telular", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 7, "estado_id"=>3, "codigo"=>"C-152", "descrip"=>"Radio receptor", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-153", "descrip"=>"Minibus", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>7, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-154", "descrip"=>"Vagoneta", "precio"=>350, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-160", "descrip"=>"Computadora portatil", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-161", "descrip"=>"Cpu", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-162", "descrip"=>"Disco duro externo", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-163", "descrip"=>"Drv", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-164", "descrip"=>"Escaner", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-165", "descrip"=>"Firewall", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-166", "descrip"=>"Impresora", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-167", "descrip"=>"Lector biometrico", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-168", "descrip"=>"Media converter", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-169", "descrip"=>"Modem", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-170", "descrip"=>"Monitor", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-171", "descrip"=>"Patch panel", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 8, "estado_id"=>3, "codigo"=>"C-172", "descrip"=>"Rack", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-173", "descrip"=>"Router", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-174", "descrip"=>"Servidor", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-175", "descrip"=>"Switch", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-176", "descrip"=>"Tablet", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>1,"grupo_id"=>8, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-177", "descrip"=>"Ups", "precio"=>2500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 2, "estado_id"=>3, "codigo"=>"C-180", "descrip"=>"Aire acondicionado", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-181", "descrip"=>"Aspiradora", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-182", "descrip"=>"Binoculares", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-183", "descrip"=>"Camara de vigilancia", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-184", "descrip"=>"Camara fotografica", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-185", "descrip"=>"Cocina", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-186", "descrip"=>"Detector de sobres bomba", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-187", "descrip"=>"Estufa", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-188", "descrip"=>"Extintores", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-189", "descrip"=>"Lustradora", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-190", "descrip"=>"Microondas", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-191", "descrip"=>"Refrigerador", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-192", "descrip"=>"Termoventilador", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);
        \App\Models\Activo::create(["responsable_id"=>2,"grupo_id"=>3, "oficina_id"=> 9, "estado_id"=>3, "codigo"=>"C-193", "descrip"=>"Ventilador", "precio"=>500, "fechaadq"=>"2012-01-02", "foto"=>$foto_activo]);

    }
}