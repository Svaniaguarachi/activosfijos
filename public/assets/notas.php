1. instalar el sistema de autenticacion con Laravel UI
	composer require laravel/ui
	php artisan ui bootstrap --auth

2. instalar adminLTE
	composer require jeroennoten/laravel-adminlte
	php artisan adminlte:install

3. Integrar la plantilla adminLTE en las vistas
	@extends('adminlte::page')

	@section('title', 'Dashboard')

	@section('content_header')
	    <h1>Dashboard</h1>
	@stop

	@section('content')
	    <p>Welcome to this beautiful admin panel.</p>
	@stop

	@section('css')
	    <link rel="stylesheet" href="/css/admin_custom.css">
	@stop

	@section('js')
	    <script> console.log('Hi!'); </script>
	@stop

4. Configurar config/adminlte.php

5. En el acrhivo DatabaseSeeder ubicado dentro de database/seeder, registramos los datos para las tablas de la base de datos, y asi cada ves que hace migrate nuestra base de datos ya tendra registros. para que cargue esos registros en el migrate ejecutar el siguiente comando.
	php artisan migrate:refresh --seed

4. Parapoder editar vistas de adminLTE como el formulario de login, ejecutar el siguiente comando.
	php artisan adminlte:install --only=main_views

6. Instalar las traduciones de laravel(para mensajes de form validation)
	composer require laravel-lang/lang:~7.0
	estas traducciones se instalan en vedndor/laravel-lang/src, y copiar la carpeta "es" dentro de resources/lang

7. configurar config/app.php para cambiar el idioma por defecto.
	'locale' => 'es',

8. Instalar dompdf para generar PDFs
	composer require barryvdh/laravel-dompdf

9. Instalar Generador de QR simplesoftware QR
	9.1 Editar el archivo composer.json y agregar esta linea en:
	"require": {
		......,
        "simplesoftwareio/simple-qrcode": "~4"
    },

    9.2 para instalar la actualizacion de simple-qrcode ejecutar en consola: composer update


