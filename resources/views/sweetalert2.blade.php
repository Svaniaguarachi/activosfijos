<script type="text/javascript">
    // mostrarAlerta(1,"Holaaaa");
    function mostrarAlerta(tipo, mensaje, titulo=null){
        const sweetalert2 = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-outline-success',
                cancelButton: 'btn btn-outline-danger'
            },
            buttonsStyling: false
        });
        sweetalert2.fire({
            position: 'top-end',
            icon: tipo,
            title: titulo,
            text: mensaje,
            timer: 10000,
            timerProgressBar: true,
        });
    }

    function alertaEliminar(mensaje, var_href=null){
        const sweetalert2 = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-outline-danger mr-3',
                cancelButton: 'btn btn-outline-success ml-3'
            },
            buttonsStyling: false
        });
        sweetalert2.fire({
            title: '¡No podrás revertir esto!',
            text: mensaje,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: '<i class="fas fa-exclamation-triangle"></i> Eliminar',
            cancelButtonText: 'Cancelar',
            timer: 5000,
            timerProgressBar: true,
        }).then((result) => {
            if (result.isConfirmed) {
                // Simula un click:
                window.location.href = var_href;
                // Simula un HTTP redirect:  Redirigir al link original para eliminar
                // window.location.replace(accion);
            }
        });
    }
</script>

@if(Session::has('mensaje_correcto'))
<script type="text/javascript">
    mostrarAlerta("success", "{{ Session::get('mensaje_correcto') }}", "{{ Session::get('titulo') }}");
</script>
@elseif(Session::has('mensaje_error'))
<script type="text/javascript">
    mostrarAlerta("error", "{{ Session::get('mensaje_error') }}", "{{ Session::get('titulo') }}");
</script>
@endif