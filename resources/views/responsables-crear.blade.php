@extends('adminlte::page')

@section('title', 'Registrar Responsable')

@section('plugins.Select2', true)

@section('content_header')
<h4 class="text-dark">Crear Responsable <a class="fw-bold" href="{{ url('responsables') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">

                    <form method="POST" action="{{ url('responsables/crear') }}" accept-charset="UTF-8" class="row g-3 needs-validation" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="nombre" class="form-label label_color">Nombre (*)</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}" required666 placeholder="Ingrese el código">
                            @error('nombre') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="ci" class="form-label label_color">C.I. (*)</label>
                            <input type="text" class="form-control" name="ci" id="ci" value="{{ old('ci') }}" required666 placeholder="Ingrese la cición">
                            @error('ci') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="ciudad" class="form-label label_color">Ciudad (*)</label>
                            <select class="form-control js-select2" name="ciudad_id" id="ciudad" required666 aria-label="Default select example">
                                <option selected disabled value="">Seleccione...</option>
                                @foreach($ciudades as $item)
                                <option value="{{ $item->id }}" @if(old('ciudad_id')==$item->id) selected @endif>{{ $item->nombre }}</option>
                                @endforeach
                            </select>
                            @error('ciudad_id')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>
                        
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="foto" class="label_color">Seleccionar foto (*)</label>
                                <p class="py- my-0">La imagen debe ser: JPG, JPEG, PNG o GIF.</p>
                                <div class="w-100 p-2" style="border: 1px solid #A7A7A7; border-style: dashed;">
                                    <input type="file" class="form-control-file mt-1 border" name="foto" id="foto" value="" accept=".png, .jpg, .jpeg, .gif" placeholder="Seleccionar imagen">
                                </div>
                            </div>
                            @error('foto')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

<script>
    $(document).ready(function() {
        $('.js-select2').select2();
    });
</script>
@stop
