@extends('adminlte::page')

@section('title', 'Registrar Grupo')
@section('plugins.Select2', true)

@section('content_header')
<h4 class="text-dark">Crear Grupo <a class="fw-bold" href="{{ url('grupos') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">

                    <form method="POST" action="{{ url('grupos/crear') }}" accept-charset="UTF-8" class="row g-3 needs-validation">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="descrip" class="form-label label_color">Descripción (*)</label>
                            <input type="text" class="form-control" name="descrip" id="descrip" value="{{ old('descrip') }}" required666 placeholder="Ingrese el código">
                            @error('descrip')
                                <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="vidautil" class="form-label label_color">Vida Útil (*)</label>
                            <select class="form-control js-select2" name="vidautil" id="vidautil" required666 aria-label="Default select example">
                                <option selected disabled value="">Seleccione...</option>
                                @foreach(range(1, 100) as $numero)
                                <option value="{{ $numero }}" @if(old('vidautil')==$numero) selected @endif>{{ $numero }}</option>
                                @endforeach
                            </select>
                            @error('vidautil')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")
<script>
    $(document).ready(function() {
        $('.js-select2').select2();
    });
</script>
@stop
