@extends('adminlte::page')

@section('title', 'Editar Usuario')

@section('content_header')
<h4 class="text-dark">Editar | Usuario <a class="fw-bold" href="{{ url('usuarios') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">
                    <form method="POST" action="{{url('usuarios/editar')}}" accept-charset="UTF-8" class="row g-3 needs-validation">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="name" class="form-label label_color">Nombre Completo (*)</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $usuario->name) }}" required666 placeholder="Ingrese su nombre completo">
                            @error('name') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="phone" class="form-label label_color">Número de teléfono (*)</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone', $usuario->phone) }}" required666 placeholder="Ingrese su número telefonico">
                            @error('phone') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="email" class="form-label label_color">Email (*)</label>
                            <input type="text" class="form-control" name="email" id="email" value="{{ old('email', $usuario->email) }}" required666 placeholder="Ingrese su correo electronico">
                            @error('email') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="role" class="form-label label_color">Rol de usuario (*)</label>
                            <select class="form-control" name="role" id="role" required666 aria-label="Default select example">
                                <option selected disabled value="">Seleccione...</option>
                                <option value="admin" @if(!old("role")&&$usuario->role=="admin") selected @elseif(old("role")=="admin") selected @endif>Admin</option>
                                <option value="subadmin" @if(!old("role")&&$usuario->role=="subadmin") selected @elseif(old("role")=="subadmin") selected @endif>SubAdmin</option>
                            </select>
                            @error('role')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="id" id="id" value="{{$usuario->id}}" hidden readonly>
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

@stop
