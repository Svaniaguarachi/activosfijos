@extends('adminlte::page')

@section('title', 'Registrar Estado')

@section('content_header')
<h4 class="text-dark">Crear Estado <a class="fw-bold" href="{{ url('estados') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">

                    <form method="POST" action="{{ url('estados/crear') }}" accept-charset="UTF-8" class="row g-3 needs-validation">
                        @csrf
                        <div class="col-md-12 mb-3">
                            <label for="descrip" class="form-label label_color">Descripción (*)</label>
                            <input type="text" class="form-control" name="descrip" id="descrip" value="{{ old('descrip') }}" required666 placeholder="Ingrese el código">
                            @error('descrip')
                                <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

@stop
