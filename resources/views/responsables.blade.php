@extends('adminlte::page')

@section('title', 'Responsables')

@section('content_header')
<h4 class="text-dark">Responsables ({{ count($responsables) }})</h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="btn btn-outline-primary" href="{{ url('responsables/crear') }}" title="Registrar nuevo activo"><i class="fas fa-plus"></i> Crear Nuevo</a>
                    <a class="btn btn-outline-danger" target="_blank" href="{{ url('responsables/pdf') }}" title="Generar reporte PDF"><i class="fas fa-file-pdf mr-1"></i> Generar Pdf</a>
                </div>
                <div class="col-12 mt-3">
                    <table class="table table-striped table-bordered " id="data_table">
                        <thead class="table-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">C.I.</th>
                                <th scope="col">Ciudad</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Editar</th>
                                <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($responsables as $key=>$item)
                            <tr>
                                <th scope="row">{{ $key+1 }}</th>
                                <td>{{ $item->nombre }}</td>
                                <td>{{ $item->ci }}</td>
                                <td>{{ $item->ciudad->nombre }}</td>
                                <td><img class="img-thumbnail" src="{{ asset('storage/responsables/'.$item->foto) }}" alt="" style="width: 80px;"></td>
                                <td>
                                    <a class="editar__item btn btn-lg btn-outline-success" href="{{ url('responsables/editar/'.$item->id) }}" title="Editar">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </td>
                                <td>
                                    <a class="eliminar__item btn btn-lg btn-outline-danger" href="{{ url('responsables/eliminar/'.$item->id) }}" onclick="return false;" title="Borrar">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
{{-- <link rel="stylesheet" href="/css/admin_custom.css"/> --}}
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

<script>
    $('#data_table').DataTable( {
        paging: true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ items por página",
            "zeroRecords": "No se encontró resultados, lo sentimos",
            "info": "Mostrando la página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
        }
    } );
</script>
<script type="text/javascript">
    $(document).on('click', '.eliminar__item', function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        alertaEliminar("¿Está seguro que desea eliminar el item?", href);
    });
</script>
@stop