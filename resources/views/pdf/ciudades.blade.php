<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reporte PDF</title>
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('vendor/adminlte/dist/css/adminlte.css') }}"> --}}
</head>
<body>
	<div style="width: 100%;">
		<img src="{{ asset('assets/imagen/logo.png') }}" style="width: 80px;">
		<h3 style="text-align: center; margin-top: -40px!important;">Reporte Ciudades</h3>
	</div>
	<div style="width: 100%; display: block; margin-top: 20px;">
		<table style="width: 100%; border-collapse: collapse;" border="1">
			<thead style="background: #28A745; color: white;">
				<tr>
					<th scope="col" style="padding: 8px;">#</th>
					<th scope="col" style="padding: 8px;">Nombre</th>
					<th scope="col" style="padding: 8px;">Fecha Creación</th>
					<th scope="col" style="padding: 8px;">Fecha Edición</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ciudades as $key=>$item)
				<tr>
					<th scope="row">{{ $key+1 }}</th>
					<td>{{ $item->nombre }}</td>
					<td>{{ $item->created_at }}</td>
					<td>{{ $item->updated_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div style="width: 100%; display: block; margin-top: 28px; font-size: 12px;">
		<span style="font-weight: bold;">Fecha:</span> {{ date("d/m/Y") }}
		<br>
		<span style="font-weight: bold;">Hora:</span> {{ date("H:i:s") }}
	</div>
</body>
</html>