<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reporte PDF</title>
</head>
<body>
	<div style="width: 100%;">
		<img src="{{ asset('assets/imagen/logo.png') }}" style="width: 80px;">
		<h3 style="text-align: center; margin-top: -40px!important;">Reporte de Usuarios</h3>
	</div>
	<div style="width: 100%; display: block;">
		<table style="width: 100%; border-collapse: collapse;" border="1">
			<thead style="background: #28A745; color: white;">
				<tr>
					<th scope="col" style="padding: 8px;">#</th>
					<th scope="col" style="padding: 8px;">Nombre</th>
					<th scope="col" style="padding: 8px;">Email</th>
					<th scope="col" style="padding: 8px;">Teléfono</th>
					<th scope="col" style="padding: 8px;">Rol de usuario</th>
					<th scope="col" style="padding: 8px;">Fecha Creación</th>
					<th scope="col" style="padding: 8px;">Fecha Edición</th>
				</tr>
			</thead>
			<tbody>
				@foreach($usuarios as $key=>$item)
				<tr>
					<th scope="row">{{ $key+1 }}</th>
					<td>{{ $item->name }}</td>
					<td>{{ $item->email }}</td>
					<td>{{ $item->phone }}</td>
					<td>{{ $item->role }}</td>
					<td>{{ $item->created_at }}</td>
					<td>{{ $item->updated_at }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div style="width: 100%; display: block; margin-top: 28px; font-size: 12px;">
		<span style="font-weight: bold;">Fecha:</span> {{ date("d/m/Y") }}
		<br>
		<span style="font-weight: bold;">Hora:</span> {{ date("H:i:s") }}
	</div>
</body>
</html>