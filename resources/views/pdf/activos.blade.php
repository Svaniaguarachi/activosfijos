<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reporte PDF</title>
</head>
<body>
	<div style="width: 100%;">
		<img src="{{ asset('assets/imagen/logo.png') }}" style="width: 80px;">
		<h3 style="text-align: center; margin-top: -40px!important;">Reporte Activos</h3>
	</div>
	<div style="width: 100%; display: block; margin-top: 30px;">
		<table style="width: 100%; border-collapse: collapse;" border="1">
			<thead style="background: #28A745; color: white;">
				<tr>
					<th scope="col" style="padding: 8px;">#</th>
					<th scope="col" style="padding: 8px;">Grupo</th>
					<th scope="col" style="padding: 8px;">Responsable</th>
					<th scope="col" style="padding: 8px;">Oficina</th>
					<th scope="col" style="padding: 8px;">Descripción</th>
					<th scope="col" style="padding: 8px;">Precio</th>
					<th scope="col" style="padding: 8px;">Fecha Adq.</th>
					<th scope="col" style="padding: 8px;">Días consumidos</th>
					<th scope="col" style="padding: 8px;">Estado</th>
					<th scope="col" style="padding: 8px;">QR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($activos as $key=>$item)
				<tr>
					<th scope="row">{{ $key+1 }}</th>
					<td>{{ $item->grupo->descrip }}</td>
					<td>{{ $item->responsable->nombre }}</td>
					<td>{{ $item->oficina->nombre }}</td>
					<td>{{ $item->descrip }}</td>
					<td>{{ $item->precio }}</td>
					<td>{{ date("d/m/Y", strtotime($item->fechaadq)) }}</td>
					<td>{{ $dias = (strtotime(date("Y-m-d")) - strtotime($item->fechaadq))/86400 }}</td>
					<td>{{ $item->estado->descrip }}</td>
					@php
						$informacion = "$item->codigo - $item->descrip - ". $item->grupo->descrip ." - $item->fechaadq";
						\QrCode::size(100)->margin(4)->generate($informacion, public_path('storage/qr/qr_'.$item->id.'.svg'));
					@endphp
					{{-- <td>{!! \QrCode::size(100)->generate('Make me into an QrCode!') !!}</td> --}}
					<td><img src="{{ asset('storage/qr/qr_'.$item->id.'.svg') }}" width="70" alt=""></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div style="width: 100%; display: block; margin-top: 28px; font-size: 12px;">
		<span style="font-weight: bold;">Fecha:</span> {{ date("d/m/Y") }}
		<br>
		<span style="font-weight: bold;">Hora:</span> {{ date("H:i:s") }}
	</div>
</body>
</html>