@extends('adminlte::page')

@section('title', 'Editar Activo')

@section('plugins.Select2', true)

@section('content_header')
<h4 class="text-dark">Editar | Activo <a class="fw-bold" href="{{ url('activos') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">
                    <form method="POST" action="{{url('activos/editar')}}" accept-charset="UTF-8" class="row g-3 needs-validation" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="codigo" class="form-label label_color">Código (*)</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" value="{{ old('codigo', $activo->codigo) }}" required666 placeholder="Ingrese el código">
                            @error('codigo') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="descrip" class="form-label label_color">Descripción (*)</label>
                            <input type="text" class="form-control" name="descrip" id="descrip" value="{{ old('descrip', $activo->descrip) }}" required666 placeholder="Ingrese la descripción">
                            @error('descrip') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="responsable" class="form-label label_color">Responsable (*)</label>
                            <select class="form-control js-select2" name="responsable_id" id="responsable" required666 aria-label="Default select example">
                                <option selected disabled value="">Seleccione...</option>
                                @foreach($responsables as $item)
                                <option value="{{ $item->id }}" @if(!old("responsable_id")&&$activo->responsable_id==$item->id) selected  @elseif(old("responsable_id")==$item->id) selected @endif>{{ $item->nombre }}</option>
                                @endforeach
                            </select>
                            @error('responsable_id')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="oficina" class="form-label label_color">Oficina (*)</label>
                            <select class="form-control js-select2" name="oficina_id" id="oficina" required666 aria-label="Default select example">
                                <option selected disabled value="">Seleccione...</option>
                                @foreach($oficinas as $item)
                                <option value="{{ $item->id }}" @if(!old("oficina_id")&&$activo->oficina_id==$item->id) selected  @elseif(old("oficina_id")==$item->id) selected @endif>{{ $item->nombre }}</option>
                                @endforeach
                            </select>
                            @error('oficina_id')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="grupo" class="label_color">Grupo (*)</label>
                            <select class="form-control js-select2" name="grupo_id" id="grupo" required666>
                                <option selected disabled value="">Seleccione...</option>
                                @foreach($grupos as $item)
                                <option value="{{ $item->id }}" @if(!old("grupo_id")&&$activo->grupo_id==$item->id) selected  @elseif(old("grupo_id")==$item->id) selected @endif>{{ $item->descrip }}</option>
                                @endforeach
                            </select>
                            @error('grupo_id')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="estado" class="label_color">Estado (*)</label>
                            <select class="form-control js-select2" name="estado_id" id="estado" required666>
                                <option selected disabled value="">Seleccione...</option>
                                @foreach($estados as $item)
                                <option value="{{ $item->id }}" @if(!old("estado_id")&&$activo->estado_id==$item->id) selected  @elseif(old("estado_id")==$item->id) selected @endif>{{ $item->descrip }}</option>
                                @endforeach
                            </select>
                            @error('estado_id')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="fechaadq" class="label_color">Fecha de adquisición (*)</label>
                            <input type="date" class="form-control" name="fechaadq" id="fechaadq" max="{{date("Y-m-d")}}" value="{{ old('fechaadq', $activo->fechaadq) }}" required666 placeholder="DD/MM/YYYY">
                            @error('fechaadq')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="nueva_foto" class="label_color">Seleccionar foto (*)</label>
                                <p class="py- my-0">La imagen debe ser: JPG, JPEG, PNG o GIF.</p>
                                <div class="w-100 p-2" style="border: 1px solid #A7A7A7; border-style: dashed;">
                                    <div class="upload_thumb image_thumb">
                                        <a href="#fotoActivo" data-toggle="modal" data-target="#fotoActivo">
                                            <img src="{{ asset('storage/activos/'.$activo->foto) }}" class="img-thumbnail" width="200" alt="..." style="width: 150px;">
                                        </a>
                                    </div>
                                    <input type="file" class="form-control-file mt-1 border" name="nueva_foto" id="nueva_foto" accept=".png, .jpg, .jpeg, .gif" placeholder="Seleccionar imagen">
                                </div>
                            </div>
                            @error('nueva_foto')<div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div>@enderror
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="foto" id="foto" value="{{$activo->foto}}" hidden readonly>
                            <input type="hidden" name="id" id="id" value="{{$activo->id}}" hidden readonly>
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="fotoActivo" data-keyboard="false" tabindex="-1" aria-labelledby="fotoActivoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <img src="{{ asset('storage/activos/'.$activo->foto) }}" class="img-thumbnail" alt="...">
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

<script>
    $(document).ready(function() {
        $('.js-select2').select2();
    });
</script>
@stop
