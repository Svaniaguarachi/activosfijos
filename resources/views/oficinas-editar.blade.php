@extends('adminlte::page')

@section('title', 'Editar Oficinas')

@section('content_header')
<h4 class="text-dark">Editar | Oficinas <a class="fw-bold" href="{{ url('oficinas') }}" style="font-size: 16px;"><i class="fas fa-long-arrow-alt-left"></i> Volver</a></h4>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-3">
                    <form method="POST" action="{{url('oficinas/editar')}}" accept-charset="UTF-8" class="row g-3 needs-validation">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="codigo" class="form-label label_color">Código (*)</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" value="{{ old('codigo', $oficina->codigo) }}" required666 placeholder="Ingrese el código">
                            @error('codigo') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="nombre" class="form-label label_color">Nombre (*)</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre', $oficina->nombre) }}" required666 placeholder="Ingrese el código">
                            @error('nombre') <div class="invalid-feedback d-block font-weight-bold">{{ $message }}</div> @enderror
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="id" id="id" value="{{$oficina->id}}" hidden readonly>
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style type="text/css">
    .label_color{color: #28A745;}
</style>
@stop

@section('js')
{{-- Incluir el archivo sweetalert2, que tiene nuestras alertas personalizadas --}}
@include("sweetalert2")

@stop
