<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Responsable extends Model
{
    use HasFactory;

    protected $table = "responsables";
    
    public function ciudad(){
        return $this->belongsTo(Ciudad::class, "ciudad_id");
    }
    public function activos(){
        return $this->hasMany(Activo::class);
    }
}
