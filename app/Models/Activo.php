<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
    use HasFactory;
    
    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }
    public function oficina(){
        return $this->belongsTo(Oficina::class);
    }
    public function estado(){
        return $this->belongsTo(Estado::class);
    }
    public function responsable(){
        return $this->belongsTo(Responsable::class);
    }
}
