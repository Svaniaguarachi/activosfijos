<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
       public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $estados = \App\Models\Estado::all();
        return view('estados', ['estados'=>$estados]);
    }

    public function create(){
        return view('estados-crear');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'descrip' => 'required|min:2|max:255',
        ]);

        $estado = new \App\Models\Estado; 
        $estado->descrip = $request->input("descrip");
        $estado->save();
        //despues de registrar el estado redireccionamos al listado de estadoes
        return redirect('estados')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $estado = \App\Models\Estado::find($id);
        if (!$estado) {
            return redirect('estados')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        return view('estados-editar', compact('estado'));
    }

    public function update(Request $request){
        $validated = $request->validate([
            'descrip' => 'required|min:2|max:255',
        ]);

        $estado = \App\Models\Estado::find($request->input("id"));
        if (!$estado) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $estado->descrip = $request->input("descrip");
        $estado->save();
        //despues de editar el estado redireccionamos al mismo formulario para ver los cambios guardados
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Estado::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $estados = \App\Models\Estado::orderBy("descrip")->get();
        $pdf = \PDF::loadView("pdf.estados", ["estados"=>$estados]);
        return $pdf->stream();
    }

}
