<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
       public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $grupos = \App\Models\Grupo::all();
        return view('grupos', ['grupos'=>$grupos]);
    }

    public function create(){
        return view('grupos-crear');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'descrip' => 'required|min:2|max:255',
            'vidautil' => 'required',
        ]);

        $grupo = new \App\Models\Grupo; 
        $grupo->descrip = $request->input("descrip");
        $grupo->vidautil = $request->input("vidautil");
        $grupo->save();
        //despues de registrar el grupo redireccionamos al listado de grupos
        return redirect('grupos')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $grupo = \App\Models\Grupo::find($id);
        if (!$grupo) {
            return redirect('grupos')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        return view('grupos-editar', compact('grupo'));
    }

    public function update(Request $request){
        $validated = $request->validate([
            'descrip' => 'required|min:2|max:255',
            'vidautil' => 'required',
        ]);

        $grupo = \App\Models\Grupo::find($request->input("id"));
        if (!$grupo) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $grupo->descrip = $request->input("descrip");
        $grupo->vidautil = $request->input("vidautil");
        $grupo->save();
        //despues de editar el grupo redireccionamos al mismo formulario para ver los cambios guardados
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Grupo::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $grupos = \App\Models\Grupo::orderBy("descrip")->get();
        $pdf = \PDF::loadView("pdf.grupos", ["grupos"=>$grupos]);
        return $pdf->stream();
    }

}
