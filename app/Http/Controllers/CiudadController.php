<?php

namespace App\Http\Controllers;

use App\Models\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $ciudades = \App\Models\Ciudad::all();
        return view('ciudades', ['ciudades'=>$ciudades]);
    }

    public function create(){
        return view('ciudades-crear');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'nombre' => 'required|min:2|max:255',
        ]);

        $ciudad = new \App\Models\Ciudad; 
        $ciudad->nombre = $request->input("nombre");
        $ciudad->save();
        //despues de registrar el ciudad redireccionamos al listado de ciudades
        return redirect('ciudades')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $ciudad = \App\Models\Ciudad::find($id);
        if (!$ciudad) {
            return redirect('ciudades')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        return view('ciudades-editar', compact('ciudad'));
    }

    public function update(Request $request){
        $validated = $request->validate([
            'nombre' => 'required|min:2|max:255',
        ]);

        $ciudad = \App\Models\Ciudad::find($request->input("id"));
        if (!$ciudad) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $ciudad->nombre = $request->input("nombre");
        $ciudad->save();
        //despues de editar el ciudad redireccioamos al listado de ciudades
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Ciudad::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $ciudades = \App\Models\Ciudad::orderBy("nombre")->get();
        $pdf = \PDF::loadView("pdf.ciudades", ["ciudades"=>$ciudades]);
        return $pdf->stream();
    }

}
