<?php

namespace App\Http\Controllers;

use App\Models\Oficina;
use Illuminate\Http\Request;

class OficinaController extends Controller
{
       public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $oficinas = \App\Models\Oficina::all();
        return view('oficinas', ['oficinas'=>$oficinas]);
    }

    public function create(){
        return view('oficinas-crear');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'codigo' => 'required|min:2|max:255',
            'nombre' => 'required|min:2|max:255',
        ]);

        $oficina = new \App\Models\Oficina; 
        $oficina->codigo = $request->input("codigo");
        $oficina->nombre = $request->input("nombre");
        $oficina->save();
        //despues de registrar el oficina redireccionamos al listado de oficinaes
        return redirect('oficinas')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $oficina = \App\Models\Oficina::find($id);
        if (!$oficina) {
            return redirect('oficinas')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        return view('oficinas-editar', compact('oficina'));
    }

    public function update(Request $request){
        $validated = $request->validate([
            'codigo' => 'required|min:2|max:255',
            'nombre' => 'required|min:2|max:255',
        ]);

        $oficina = \App\Models\Oficina::find($request->input("id"));
        if (!$oficina) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $oficina->codigo = $request->input("codigo");
        $oficina->nombre = $request->input("nombre");
        $oficina->save();
        //despues de editar el oficina redireccionamos al mismo formulario para ver los cambios guardados
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Oficina::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $oficinas = \App\Models\Oficina::orderBy("nombre")->get();
        $pdf = \PDF::loadView("pdf.oficinas", ["oficinas"=>$oficinas]);
        return $pdf->stream();
    }

}
