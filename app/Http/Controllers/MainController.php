<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $usuarios = \App\Models\User::orderBy('name')->get();
        return view('usuario/usuarios', compact('usuarios'));
    }

    public function create(){
        // return \QrCode::size(255)->generate("código-nombre-tipofechaingreso");
        return view('usuario/usuarios-crear');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|digits_between:7,8',
            'role' => 'required',
        ]);

        $usuario = new \App\Models\User; 
        $usuario->name = $request->input("name");
        $usuario->email = $request->input("email");
        $usuario->phone = $request->input("phone");
        $usuario->role = $request->input("role");
        $usuario->password = \Hash::make($request->input("12345678"));
        $usuario->save();
        //despues de registrar el usuario redireccionamos al listado de usuarios
        return redirect('usuarios')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $usuario = \App\Models\User::find($id);
        if (!$usuario) {
            return redirect('usuarios')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        return view('usuario/usuarios-editar', compact('usuario'));
    }

    public function update(Request $request){
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|digits_between:7,8',
            'role' => 'required',
        ]);

        $usuario = \App\Models\User::find($request->input("id"));
        if (!$usuario) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $usuario->name = $request->input("name");
        $usuario->email = $request->input("email");
        $usuario->phone = $request->input("phone");
        $usuario->role = $request->input("role");
        $usuario->save();
        //despues de editar el usuario redireccioamos al listado de usuarios
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\User::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado","mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $usuarios = \App\Models\User::orderBy("name")->get();
        $pdf = \PDF::loadView("pdf.usuarios", ["usuarios"=>$usuarios]);
        return $pdf->stream();
    }



    /*Funciones para actualizar datos de cuenta de usuario/perfil */
    public function editProfile(){
        $usuario = \Auth::user();
        return view('usuario/editar-cuenta', compact('usuario'));
    }
    
    public function updateProfile(Request $request){
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|digits_between:7,8',
            'email' => 'required|email',
        ]);
        $usuario = \Auth::user();
        $usuario->name = $request->input("name");
        $usuario->phone = $request->input("phone");
        $usuario->email = $request->input("email");
        $usuario->save();
        //despues de editar el cuenta de usuario redireccioamos al home
        return redirect('/')->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"Los datos de tu cuenta fueron actualizados correctamente."]);
    }

    public function updatePassword(Request $request){
        $validated = $request->validate([
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
        \App\Models\User::find(auth()->user()->id)->update(['password'=> \Hash::make($request->input('password'))]);
        //despues de cambiar password de la cuenta de usuario redireccioamos al home
        return redirect('/')->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"Su contraseña fue actualizada correctamente."]);
    }

    public function logout(){
        \Auth::logout();
        return redirect("login");
    }
}
