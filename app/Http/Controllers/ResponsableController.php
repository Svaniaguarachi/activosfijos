<?php

namespace App\Http\Controllers;

use App\Models\Responsable;
use Illuminate\Http\Request;

class ResponsableController extends Controller
{
       public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $responsables = \App\Models\Responsable::all();
        return view('responsables', ['responsables'=>$responsables]);
    }

    public function create(){
        $ciudades = \App\Models\Ciudad::orderBy("nombre")->get();
        return view('responsables-crear', compact("ciudades"));
    }

    public function store(Request $request){
        $validated = $request->validate([
            'nombre' => 'required|min:2|max:255',
            'ci' => 'required|min:2|max:255',
            'ciudad_id' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);

        $responsable = new \App\Models\Responsable; 
        $responsable->nombre = $request->input("nombre");
        $responsable->ci = $request->input("ci");
        $responsable->ciudad_id = $request->input("ciudad_id");
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $ruta = public_path('storage/responsables');
            $extension = $foto->getClientOriginalExtension();
            $nombre_foto = time(). time(). "_responsables.".$extension;
            $archivo_subido = $request->file('foto')->move($ruta, $nombre_foto);
            $responsable->foto = $nombre_foto;
        }
        $responsable->save();
        //despues de registrar el responsable redireccionamos al listado de responsablees
        return redirect('responsables')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $responsable = \App\Models\Responsable::find($id);
        if (!$responsable) {
            return redirect('responsables')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        $ciudades = \App\Models\Ciudad::orderBy("nombre")->get();
        return view('responsables-editar', ['responsable'=>$responsable, 'ciudades'=>$ciudades]);
    }

    public function update(Request $request){
        $validated = $request->validate([
            'nombre' => 'required|min:2|max:255',
            'ci' => 'required|min:2|max:255',
            'ciudad_id' => 'required',
            'nueva_foto' => 'nullable|mimes:jpeg,png,jpg,gif',
        ]);

        $responsable = \App\Models\Responsable::find($request->input("id"));
        if (!$responsable) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $responsable->nombre = $request->input("nombre");
        $responsable->ci = $request->input("ci");
        $responsable->ciudad_id = $request->input("ciudad_id");
        if ($request->hasFile('nueva_foto')) {
            $nueva_foto = $request->file('nueva_foto');
            $extension = $nueva_foto->getClientOriginalExtension();
            $ruta = public_path('storage/responsables');
            $nombre_foto = time(). time(). "_responsables.".$extension;
            $archivo_subido = $request->file('nueva_foto')->move($ruta, $nombre_foto);
            $responsable->foto = $nombre_foto;
        }
        $responsable->save();
        //despues de editar el responsable redireccionamos al mismo formulario para ver los cambios guardados
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Responsable::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $responsables = \App\Models\Responsable::orderBy("nombre")->get();
        $pdf = \PDF::loadView("pdf.responsables", ["responsables"=>$responsables]);
        return $pdf->stream();
    }

}
