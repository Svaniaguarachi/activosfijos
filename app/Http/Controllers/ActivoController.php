<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActivoController extends Controller{

    public function __construct(){
        //Esto valida que para acceder a todos los metodos de este controlador se debe estar logueado
        $this->middleware('auth');
    }
    
    public function index(){
        $activos = \App\Models\Activo::all();
        return view('activos', ['activos'=>$activos]);
    }

    public function create(){
        $responsables = \App\Models\Responsable::orderBy('nombre')->get();
        $grupos = \App\Models\Grupo::orderBy('descrip')->get();
        $oficinas = \App\Models\Oficina::orderBy('nombre')->get();
        $estados = \App\Models\Estado::orderBy('descrip')->get();
        return view('activos-crear', ['responsables'=>$responsables, 'grupos'=>$grupos, 'oficinas'=>$oficinas, 'estados'=>$estados]);
    }

    public function store(Request $request){
        $validated = $request->validate([
            'codigo' => 'required|min:2|max:255',
            'descrip' => 'required|min:2|max:255',
            'responsable_id' => 'required',
            'oficina_id' => 'required',
            'grupo_id' => 'required',
            'estado_id' => 'required',
            'fechaadq' => 'required|date',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);

        $activo = new \App\Models\Activo; 
        $activo->codigo = $request->input("codigo");
        $activo->descrip = $request->input("descrip");
        $activo->responsable_id = $request->input("responsable_id");
        $activo->oficina_id = $request->input("oficina_id");
        $activo->grupo_id = $request->input("grupo_id");
        $activo->estado_id = $request->input("estado_id");
        $activo->fechaadq = $request->input("fechaadq");
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $ruta = public_path('storage/activos');
            $extension = $foto->getClientOriginalExtension();
            $nombre_foto = time(). time(). "_activos.".$extension;
            $archivo_subido = $request->file('foto')->move($ruta, $nombre_foto);
            $activo->foto = $nombre_foto;
        }
        $activo->save();
        //despues de registrar el activo redireccionamos al listado de activos
        return redirect('activos')->with(["titulo"=>"Registro Exitoso", "mensaje_correcto"=>"El item fue creado correctamente."]);
    }

    public function show($id){

    }

    public function edit($id){
        $activo = \App\Models\Activo::find($id);
        if (!$activo) {
            return redirect('activos')->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere editar."]);
        }
        $responsables = \App\Models\Responsable::orderBy('nombre')->get();
        $grupos = \App\Models\Grupo::orderBy('descrip')->get();
        $oficinas = \App\Models\Oficina::orderBy('nombre')->get();
        $estados = \App\Models\Estado::orderBy('descrip')->get();
        return view('activos-editar', ['activo'=>$activo, 'responsables'=>$responsables, 'grupos'=>$grupos, 'oficinas'=>$oficinas, 'estados'=>$estados]);
    }

    public function update(Request $request){
        $validated = $request->validate([
            'codigo' => 'required|min:2|max:255',
            'descrip' => 'required|min:2|max:255',
            'responsable_id' => 'required',
            'oficina_id' => 'required',
            'grupo_id' => 'required',
            'estado_id' => 'required',
            'fechaadq' => 'required|date',
            'nueva_foto' => 'nullable|mimes:jpeg,png,jpg,gif',
        ]);

        $activo = \App\Models\Activo::find($request->input("id"));
        if (!$activo) {
            return redirect()->back()->with(["titulo"=>"Incorrecto!", "mensaje_error"=>"No existe el item que quiere actualizar."]);
        }
        $activo->codigo = $request->input("codigo");
        $activo->descrip = $request->input("descrip");
        $activo->responsable_id = $request->input("responsable_id");
        $activo->oficina_id = $request->input("oficina_id");
        $activo->grupo_id = $request->input("grupo_id");
        $activo->estado_id = $request->input("estado_id");
        $activo->fechaadq = $request->input("fechaadq");
        if ($request->hasFile('nueva_foto')) {
            $nueva_foto = $request->file('nueva_foto');
            $extension = $nueva_foto->getClientOriginalExtension();
            $ruta = public_path('storage/activos');
            $nombre_foto = time(). time(). "_activos.".$extension;
            $archivo_subido = $request->file('nueva_foto')->move($ruta, $nombre_foto);
            $activo->foto = $nombre_foto;
        }
        $activo->save();
        //despues de editar el activo redireccioamos al listado de activos
        return redirect()->back()->with(["titulo"=>"Actualización Exitosa", "mensaje_correcto"=>"El item fue actualizado correctamente."]);
    }

    public function destroy($id){
        \App\Models\Activo::where("id", $id)->delete();
        // enviamos variables de session para mostrar en nuestras alertas
        return redirect()->back()->with(["titulo"=>"Registro Eliminado", "mensaje_correcto"=>"El item fue eliminado correctamente."]);
    }

    public function pdf(){
        $activos = \App\Models\Activo::orderBy("id")->get();
        $pdf = \PDF::loadView("pdf.activos", ["activos"=>$activos]);
        $pdf->setPaper("a4", "landscape");
        return $pdf->stream();
    }

}
