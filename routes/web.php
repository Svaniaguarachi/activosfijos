<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ActivoController;
use App\Http\Controllers\CiudadController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\OficinaController;
use App\Http\Controllers\ResponsableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ActivoController::class, 'index']);

Auth::routes();
Route::get('account/logout', [MainController::class, 'logout']);
Route::get('profile/username', [MainController::class, 'editProfile']);
Route::post('profile/editar', [MainController::class, 'updateProfile']);
Route::post('profile/cambiar-password', [MainController::class, 'updatePassword']);
Route::get('usuarios', [MainController::class, 'index']);
Route::get('usuarios/crear', [MainController::class, 'create']);
Route::post('usuarios/crear', [MainController::class, 'store']);
Route::get('usuarios/editar/{id}', [MainController::class, 'edit']);
Route::post('usuarios/editar', [MainController::class, 'update']);
Route::get('usuarios/eliminar/{id}', [MainController::class, 'destroy']);
Route::get('usuarios/pdf', [MainController::class, 'pdf']);

Route::get('activos', [ActivoController::class, 'index']);
Route::get('activos/crear', [ActivoController::class, 'create']);
Route::post('activos/crear', [ActivoController::class, 'store']);
Route::get('activos/editar/{id}', [ActivoController::class, 'edit']);
Route::post('activos/editar', [ActivoController::class, 'update']);
Route::get('activos/eliminar/{id}', [ActivoController::class, 'destroy']);
Route::get('activos/pdf', [ActivoController::class, 'pdf']);

Route::get('ciudades', [CiudadController::class, 'index']);
Route::get('ciudades/crear', [CiudadController::class, 'create']);
Route::post('ciudades/crear', [CiudadController::class, 'store']);
Route::get('ciudades/editar/{id}', [CiudadController::class, 'edit']);
Route::post('ciudades/editar', [CiudadController::class, 'update']);
Route::get('ciudades/eliminar/{id}', [CiudadController::class, 'destroy']);
Route::get('ciudades/pdf', [CiudadController::class, 'pdf']);

Route::get('estados', [EstadoController::class, 'index']);
Route::get('estados/crear', [EstadoController::class, 'create']);
Route::post('estados/crear', [EstadoController::class, 'store']);
Route::get('estados/editar/{id}', [EstadoController::class, 'edit']);
Route::post('estados/editar', [EstadoController::class, 'update']);
Route::get('estados/eliminar/{id}', [EstadoController::class, 'destroy']);
Route::get('estados/pdf', [EstadoController::class, 'pdf']);

Route::get('grupos', [GrupoController::class, 'index']);
Route::get('grupos/crear', [GrupoController::class, 'create']);
Route::post('grupos/crear', [GrupoController::class, 'store']);
Route::get('grupos/editar/{id}', [GrupoController::class, 'edit']);
Route::post('grupos/editar', [GrupoController::class, 'update']);
Route::get('grupos/eliminar/{id}', [GrupoController::class, 'destroy']);
Route::get('grupos/pdf', [GrupoController::class, 'pdf']);

Route::get('oficinas', [OficinaController::class, 'index']);
Route::get('oficinas/crear', [OficinaController::class, 'create']);
Route::post('oficinas/crear', [OficinaController::class, 'store']);
Route::get('oficinas/editar/{id}', [OficinaController::class, 'edit']);
Route::post('oficinas/editar', [OficinaController::class, 'update']);
Route::get('oficinas/eliminar/{id}', [OficinaController::class, 'destroy']);
Route::get('oficinas/pdf', [OficinaController::class, 'pdf']);

Route::get('responsables', [ResponsableController::class, 'index']);
Route::get('responsables/crear', [ResponsableController::class, 'create']);
Route::post('responsables/crear', [ResponsableController::class, 'store']);
Route::get('responsables/editar/{id}', [ResponsableController::class, 'edit']);
Route::post('responsables/editar', [ResponsableController::class, 'update']);
Route::get('responsables/eliminar/{id}', [ResponsableController::class, 'destroy']);
Route::get('responsables/pdf', [ResponsableController::class, 'pdf']);


Auth::routes();
Route::get('/home', [ActivoController::class, 'index']);
